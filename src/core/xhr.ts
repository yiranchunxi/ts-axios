import { AxiosRequestConfig, AxiosPromise, AxiosResponse } from '../types'
import {buildURL} from '../helpers/url'
import { parseHeaders } from '../helpers/header'
import {createError} from '../helpers/error'

export  default function xhr(config:AxiosRequestConfig):AxiosPromise {

  return  new Promise(((resolve,reject) => {
    // 解构
    const {data=null,url,method='get',headers,responseType,timeout,cancelToken}=config

    const request=new XMLHttpRequest()

    if(responseType){
      request.responseType=responseType
    }

    if(timeout){
      request.timeout=timeout
    }

    request.open(method.toUpperCase(),url!,true)

    request.onreadystatechange=function handleLoad(){
      if(request.readyState !==4 ){
        return
      }
      // 处理状态码
      if(request.status === 0){
        return
      }


      const responseHeaders=parseHeaders(request.getAllResponseHeaders())
      const responseData = responseType !== 'text' ? request.response : request.responseText
      const response:AxiosResponse={
        data:responseData,
        status:request.status,
        statusText:request.statusText,
        headers:responseHeaders,
        config,
        request
      }

      handleResponse(response)
    }


    request.onerror=function handleError(){
      // reject(new Error('NetWork1 Error'))
      reject(createError('NetWork1 Error',config,null,request))
    }


    request.ontimeout=function handleTimeout(){
      // reject(new Error(`Timeout1 of ${timeout} ms exceeded`))
      reject(createError(`Timeout1 of ${timeout} ms exceeded`,config,'ECONNABORTED',request))
    }

    Object.keys(headers).forEach((name)=>{
      // 如果数据为空的时候
      if(data === null && name.toLocaleLowerCase() === 'content-type'){
        delete  headers[name]
      }else{
        request.setRequestHeader(name,headers[name])
      }
    })

    // 取消逻辑
    if(cancelToken){
      cancelToken.promise.then(reason => {
        request.abort()
        reject(reason)
      })
    }

    request.send(data)


    function handleResponse(response: AxiosResponse):void{
      if(response.status>=200 && response.status<300){
        resolve(response)
      }else{
        // reject(new Error(`Request1 failed with status code ${response.status}`))
        reject(createError(`Request1 failed with status code ${response.status}`,config,null,request,response))
      }
    }
  }))

}
