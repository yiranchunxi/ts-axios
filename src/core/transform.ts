// transform 逻辑重构

import { AxiosTransformer } from '../types'

export default  function transform(data: any, headers:any, fns?:AxiosTransformer | AxiosTransformer[]): any {
  if(!fns){
    return data
  }

  if(!Array.isArray(fns)){
    fns=[fns]
  }
  fns.forEach(fn=>{
    console.log(fn)
    data = fn(data,headers)
  })

  return  data
}
