export  type Method='get' | 'GET' | 'delete' | 'DELETE' | 'head' | 'HEAD' | 'options' | 'OPTIONS' | 'post' | 'POST'|'put'|'PUT'|'patch'|'PATCH'
export  interface AxiosRequestConfig{
  url?: string
  method?: Method
  data?: any
  params?: any
  headers?: any
  responseType?:XMLHttpRequestResponseType
  timeout?:number
  [propName: string]: any
  transformRequest?: AxiosTransformer | AxiosTransformer[]
  transformResponse?: AxiosTransformer | AxiosTransformer[]
  cancelToken?: CancelToken
}

export interface AxiosResponse<T = any> {
  data:T
  status:number
  statusText:string
  headers:any
  config:AxiosRequestConfig
  request:any
}

export interface AxiosPromise<T = any> extends Promise<AxiosResponse<T>>{

}


export interface AxiosError extends Error{
  config:AxiosRequestConfig
  code?:string
  request?:any
  response?:AxiosResponse
  isAxiosError:boolean
}

export interface Axios {
  defaults: AxiosRequestConfig
  interceptors:{
    request: AxiosInterceptorManager<AxiosRequestConfig>
    response: AxiosInterceptorManager<AxiosResponse>
  }

  request<T = any>(config:AxiosRequestConfig):AxiosPromise<T>

  get<T = any>(url: string, config?: AxiosRequestConfig):AxiosPromise<T>

  delete<T = any>(url: string, config?:AxiosRequestConfig):AxiosPromise<T>

  head<T = any>(url: string, config?:AxiosRequestConfig):AxiosPromise<T>

  options<T = any>(url: string, config?:AxiosRequestConfig):AxiosPromise<T>

  post<T = any>(url: string, data?: any, config?:AxiosRequestConfig):AxiosPromise<T>

  put<T = any>(url: string, data?: any, config?:AxiosRequestConfig):AxiosPromise<T>

  patch<T = any>(url: string, data?: any, config?:AxiosRequestConfig):AxiosPromise<T>
}
// 混合方法
export interface AxiosInstance extends Axios{
  <T = any>(config: AxiosRequestConfig):AxiosPromise<T>

  <T = any>(url: string,config?: AxiosRequestConfig):AxiosPromise<T>
}

// 拦截器管理对象
export interface AxiosInterceptorManager<T> {
  use(resolved: ResolvedFn<T>, rejected?: RejectedFn):number

  eject(id: number): void
}

export interface ResolvedFn<T=any> {
  (val: T): T | Promise<T>
}

export interface RejectedFn {
  (error: any): any
}

export interface AxiosTransformer {
  (data: any,headers?: any):any
}

// 静态方法拓展
export interface AxiosStatic extends AxiosInstance{
  create(config?: AxiosRequestConfig): AxiosInstance

  CancelToken: CancelTokenStatic
  Cancel: CancelStatic
  isCancel: (value: any) => boolean
}

// 取消请求CancelToken
export interface CancelToken {
  promise: Promise<Cancel>
  reason?: Cancel
  throwIfRequested(): void
}
// Canceler 是取消方法的接口定义
export interface Canceler {
  (message?: string): void
}

// CancelExecutor 是 CancelToken 类构造函数参数的接口定义
export interface CancelExecutor {
  (cancel: Canceler): void
}

export interface CancelTokenSource {
  token: CancelToken
  cancel: Canceler
}

export  interface CancelTokenStatic {
  new (executor: CancelExecutor): CancelToken

  source(): CancelTokenSource
}

// Cancel 类实现及 axios 的扩展
// 是实例类型的接口定义
export interface Cancel {
  message?: string
}
// 是类类型的接口定义
export interface CancelStatic {
  new(message?: string): Cancel
}
