import { AxiosRequestConfig, AxiosStatic } from './types'
import Axios from './core/Axios'
import { extend } from './helpers/util'
import defaults from './defaults'
import mergeConfig from './core/mergeConfig'
import CancelToken from './cancel/CancelToken'
import Cancel, { isCancel } from './cancel/Cancel'
// 当直接调用 axios 方法就相当于执行了 Axios 类的 request 方法发送请求
function createInstance(config: AxiosRequestConfig):AxiosStatic {
  const context = new Axios(config)
  // 返回一个原函数的拷贝，并拥有指定的 this 值和初始参数。
  const instance =Axios.prototype.request.bind(context)
  extend(instance,context)
  return instance as AxiosStatic
}


const axios=createInstance(defaults)

axios.create = function create(config) {
  return createInstance(mergeConfig(defaults,config))
}
axios.CancelToken=CancelToken
axios.Cancel=Cancel
axios.isCancel=isCancel
export  default axios
