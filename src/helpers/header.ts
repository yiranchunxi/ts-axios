import { deepMerge, isPlainObject } from './util'
import has = Reflect.has
import { Method } from '../types'

/**
 * 请求头规范化
 */
function normalizeHeaderName(headers:any,normalizedName:string):void {
  if(!headers){
    return
  }
  Object.keys(headers).forEach((name)=> {
     if(name!==normalizedName && name.toUpperCase() === normalizedName.toUpperCase()){
       headers[normalizedName]=headers[name]
       delete headers[name]
     }
  })
}

export function processHeaders(headers:any,data:any):any {
  normalizeHeaderName(headers,'Content-Type')
  if(isPlainObject(data)){
    if(headers && !headers['Content-Type']){
      headers['Content-Type']='application/json;charset=utf-8'
    }
  }
  return headers
}

export function parseHeaders(headers:string):any {
  let parsed = Object.create(null)
  if(!headers){
    return parsed
  }

  headers.split('\r\n').forEach(line=>{
    let [key,val]=line.split(':')
    key=key.trim().toLowerCase()
    if(!key){
      return
    }
    if(val){
      val=val.trim()
    }
    parsed[key]=val
  })

  return parsed
}

// 经过合并后的配置中的 headers 是一个复杂对象，多了 common、post、get 等属性，而这些属性中的值才是我们要真正添加到请求 header 中的。

export function flattenHeaders(headers: any, method: Method):any {
  if(!headers){
    return headers
  }
  headers = deepMerge(headers.common || {}, headers[method] || {}, headers)

  const methodsToDelete = ['delete', 'get', 'head', 'options', 'post', 'put', 'patch', 'common']

  methodsToDelete.forEach(method => {
    delete headers[method]
  })

  return  headers
}
