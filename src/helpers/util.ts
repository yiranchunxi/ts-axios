import { type } from 'os'

const toString=Object.prototype.toString
// 用户自定义的类型保护
export function isDate(val:any):val is Date{
  return  toString.call(val) === '[object Date]'
}

/*export function isObject(val:any):val is Object {
  return val!==null &&typeof val ==='object'
}*/
// object 表示非原始类型 Object 是一组数据和功能的集合  判断是否是普通对象
export function isPlainObject(val:any):val is Object{
  // console.log(val)
  // console.log(toString.call(val))
  return  toString.call(val) === '[object Object]'
}

export function extend<T ,U>(to:T, from:U): T & U{
   for(const key in from){
     ;(to as T & U)[key] = from[key] as any
   }
   // console.log(from)
   return to as T & U
}

export  function  deepMerge(...objs: any[]): any {
  const result = Object.create(null)

  objs.forEach(obj =>{
    if(obj){
      Object.keys(obj).forEach(key =>{
        const val = obj[key]
        if(isPlainObject(val)){
          if(isPlainObject(val)){
            result[key] = deepMerge(result[key], val)
          }else{
            result[key] = deepMerge(val)
          }
        }else{
          result[key] =val
        }
      })
    }
  })

  return result
}
